export  interface IGameObjectResponse{
    hasWon: boolean;
    gameId: string;
    playerCircle: Player;
    playerCross: Player;
    gameScene: IGameScene;
}
export interface IGameScene{
    positions: string [];
}
export interface Player {
    playerId: string;
    username: string;
}