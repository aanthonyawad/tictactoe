export interface IsceneRequest{
    playerId : string;
    placeId : string;
    gameId : string;
}
export interface IJoinGameRequest{
    gameId : string;
    username:string;
}

export interface ICreateGameRequest{
    username:string;
}