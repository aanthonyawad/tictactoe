const errorMessages = {
    cannotJoin:{
        message:'Session is full Cannot Join.'   
    },
    gameIdNotFound:{
        message:'Cannot Find Game Session.'
    },
    playerNotFound:{
        message:'Player Cannot Play In This Game'
    }
}