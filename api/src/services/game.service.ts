import { ICreateGameRequest } from './../models/requests/requests.models';
import { Injectable } from '@nestjs/common';
import {
  IJoinGameRequest,
  IsceneRequest,
} from 'src/models/requests/requests.models';
import { IGameObjectResponse } from 'src/models/responses/responses.models';
import { Uuid, UuidOptions } from 'node-ts-uuid';
import * as _ from "lodash"
@Injectable()
export class GameService {
  //for simplicity i will not add DB neither JWT support i will save data in an array
  gameObjectResponses: IGameObjectResponse[] = [];
  joinGame(joinGameRequest: IJoinGameRequest): IGameObjectResponse {
    let gameObjectResponse = this.findGame(joinGameRequest.gameId);
    if(!gameObjectResponse){
      throw new Error('gameIdNotFound');
    }
    if(this.checkExistantPlayerCircle(gameObjectResponse))
      throw new Error('canontJoin');
    gameObjectResponse.playerCircle = {
      playerId:this.createUUid('circle'),
      username:joinGameRequest.username
    }
    return gameObjectResponse;
  }

  play(sceneRequest: IsceneRequest): IGameObjectResponse {
    let gameObjectResponse = this.findGame(sceneRequest.gameId);
    if(!gameObjectResponse){
      throw new Error('gameIdNotFound');
    }
    if(this.checkCorrectPlayerCross(gameObjectResponse,sceneRequest.playerId)){

    }if(this.checkCorrectPlayerCircle(gameObjectResponse,sceneRequest.playerId)){
      
    }
    else {
      throw new Error('playerNotFound');
    }
    return gameObjectResponse;
  }

  createNewGame(createGameRequest: ICreateGameRequest): IGameObjectResponse {
    const gameObjectResponse: IGameObjectResponse = {
      hasWon: false,
      gameId: this.createUUid('game'),
      playerCross: {
        playerId:this.createUUid('cross'),
        username:createGameRequest.username
      },
      playerCircle:{
        playerId:'',
        username:''
      } ,
      gameScene: {
        positions: [],
      },
    };
    this.gameObjectResponses.push(gameObjectResponse);
    return gameObjectResponse;
  }

  private createUUid(prefix: string): string {
    const options: UuidOptions = {
      length: 50,
      prefix: `${prefix}-`,
    };
    return Uuid.generate(options);
  }
  
  private findGame(gameId: string) :IGameObjectResponse {
    let foundGame: IGameObjectResponse = null;
    if(this.gameObjectResponses.length == 0)
      return null;
      this.gameObjectResponses.forEach(element =>{
        if(element.gameId === gameId){
          foundGame = element;
        }
      }) 
      return foundGame;
  }

  private checkExistantPlayerCircle(gameObjectResponse : IGameObjectResponse): boolean{
    if(gameObjectResponse.playerCircle.playerId.length !== 0){
      return true;
    }
    return false;
  }

  
  private checkCorrectPlayerCross(gameObjectResponse : IGameObjectResponse,playerId : string ): boolean{
    if(gameObjectResponse.playerCross.playerId.length !== 0 
      && gameObjectResponse.playerCross.playerId === playerId){
      return true;
    }
    return false;
  }

  private checkCorrectPlayerCircle(gameObjectResponse : IGameObjectResponse,playerId : string ): boolean{
    if(gameObjectResponse.playerCross.playerId.length !== 0 
      && gameObjectResponse.playerCross.playerId === playerId){
      return true;
    }
    return false;
  }
}
