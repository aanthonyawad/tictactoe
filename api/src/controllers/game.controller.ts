import { Body, Controller, Get, Post, Put } from '@nestjs/common';
import { GameService } from '../services/game.service';
import { IGameObjectResponse } from 'src/models/responses/responses.models';
import {
  ICreateGameRequest,
  IJoinGameRequest,
  IsceneRequest,
} from 'src/models/requests/requests.models';

@Controller('game')
export class GameController {
  constructor(private readonly gameService: GameService) {}

  @Post('new')
  postNew(@Body() createGameRequest: ICreateGameRequest): IGameObjectResponse {
    return this.gameService.createNewGame(createGameRequest);
  }

  @Put('join')
  postJoin(@Body() joinGameRequest: IJoinGameRequest): IGameObjectResponse {
    return this.gameService.joinGame(joinGameRequest);
  }

//   @Put('play')
//   putPlay(@Body() sceneRequest: IsceneRequest): IGameObjectResponse {
//     return this.gameService.play(sceneRequest);
//   }
}
